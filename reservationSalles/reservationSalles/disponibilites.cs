﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace reservationSalles2018
{
    public partial class frmDisponibilites : Form
    {

        DataTable tableTypes;
        public frmDisponibilites()
        {
            InitializeComponent();
        }

        private void frmDisponibilites_Load(object sender, EventArgs e)
        {
            tableTypes = frmM2LReservationSalles.reservationsSallesDataSet.Tables["Types"];

            cbxTypeSalles.DataSource = tableTypes;
            cbxTypeSalles.DisplayMember = tableTypes.Columns[1].ToString();
            cbxTypeSalles.ValueMember = tableTypes.Columns[0].ToString();



            cbxLPlages.Items.Add("Matin");
            cbxLPlages.Items.Add("Après midi");
            cbxLPlages.SelectedIndex = 0;
            textBox1.MaxLength = 3;
            textBox2.MaxLength = 3;
        }

        private void cbxTypeSalles_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void cbxLPlages_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void btnRecherche_Click(object sender, EventArgs e)
        {
            short CapaMini;
            if (Int16.TryParse(textBox1.Text, out CapaMini))
            {
                if (CapaMini < 1 || CapaMini > 300)
                {
                    textBox1.Text = null;
                    textBox1.Focus();
                    MessageBox.Show("la capacité saisie doit être entre 1 et 300");
                }
            }
            short CapaMaxi;
            if (Int16.TryParse(textBox2.Text, out CapaMaxi))
            {
                if (CapaMaxi < 1 || CapaMaxi > 300 || CapaMaxi < CapaMini)
                {
                   
                    if (CapaMaxi < 1 || CapaMaxi > 300)
                    {
                        textBox2.Text = null;
                        textBox2.Focus();
                        MessageBox.Show("la capacité saisie doit être entre 1 et 300");
                    }
                    else
                    {
                        MessageBox.Show("La capacité doit être inferieur à la capacité maximale");
                    }
                }
            }

        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back)
            {
                e.Handled = true;
            }
        }
    }
}


