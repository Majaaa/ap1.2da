﻿namespace reservationSalles2018
{
    partial class frmFacturationReservations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lblMoisReservations = new System.Windows.Forms.Label();
            this.cbxLigueReservations = new System.Windows.Forms.ComboBox();
            this.lblLiguesReservations = new System.Windows.Forms.Label();
            this.dtpMoisReservations = new System.Windows.Forms.DateTimePicker();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.lblMontant = new System.Windows.Forms.Label();
            this.lblNombreReservations = new System.Windows.Forms.Label();
            this.lblMontantTotalReservations = new System.Windows.Forms.Label();
            this.lblNombreTotalReservations = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(374, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(304, 33);
            this.label1.TabIndex = 71;
            this.label1.Text = "Gestion réservations";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // lblMoisReservations
            // 
            this.lblMoisReservations.AutoSize = true;
            this.lblMoisReservations.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.lblMoisReservations.Location = new System.Drawing.Point(56, 97);
            this.lblMoisReservations.Name = "lblMoisReservations";
            this.lblMoisReservations.Size = new System.Drawing.Size(142, 18);
            this.lblMoisReservations.TabIndex = 72;
            this.lblMoisReservations.Text = "Choissisez le mois :";
            // 
            // cbxLigueReservations
            // 
            this.cbxLigueReservations.FormattingEnabled = true;
            this.cbxLigueReservations.Location = new System.Drawing.Point(59, 213);
            this.cbxLigueReservations.Name = "cbxLigueReservations";
            this.cbxLigueReservations.Size = new System.Drawing.Size(167, 21);
            this.cbxLigueReservations.TabIndex = 73;
            this.cbxLigueReservations.SelectedIndexChanged += new System.EventHandler(this.cbxLigueReservations_SelectedIndexChanged);
            // 
            // lblLiguesReservations
            // 
            this.lblLiguesReservations.AutoSize = true;
            this.lblLiguesReservations.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.lblLiguesReservations.Location = new System.Drawing.Point(56, 183);
            this.lblLiguesReservations.Name = "lblLiguesReservations";
            this.lblLiguesReservations.Size = new System.Drawing.Size(139, 18);
            this.lblLiguesReservations.TabIndex = 74;
            this.lblLiguesReservations.Text = "Choissisez la ligue :";
            this.lblLiguesReservations.Click += new System.EventHandler(this.cbxLiguesReservations_Click);
            // 
            // dtpMoisReservations
            // 
            this.dtpMoisReservations.Location = new System.Drawing.Point(59, 129);
            this.dtpMoisReservations.Name = "dtpMoisReservations";
            this.dtpMoisReservations.Size = new System.Drawing.Size(200, 20);
            this.dtpMoisReservations.TabIndex = 75;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(425, 97);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(607, 297);
            this.dataGridView1.TabIndex = 76;
            // 
            // lblMontant
            // 
            this.lblMontant.AutoSize = true;
            this.lblMontant.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.lblMontant.Location = new System.Drawing.Point(53, 307);
            this.lblMontant.Name = "lblMontant";
            this.lblMontant.Size = new System.Drawing.Size(215, 18);
            this.lblMontant.TabIndex = 77;
            this.lblMontant.Text = "Montant total des réservations :";
            // 
            // lblNombreReservations
            // 
            this.lblNombreReservations.AutoSize = true;
            this.lblNombreReservations.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.lblNombreReservations.Location = new System.Drawing.Point(56, 360);
            this.lblNombreReservations.Name = "lblNombreReservations";
            this.lblNombreReservations.Size = new System.Drawing.Size(199, 18);
            this.lblNombreReservations.TabIndex = 78;
            this.lblNombreReservations.Text = "Nombre total de réservation :";
            // 
            // lblMontantTotalReservations
            // 
            this.lblMontantTotalReservations.AutoSize = true;
            this.lblMontantTotalReservations.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.lblMontantTotalReservations.Location = new System.Drawing.Point(295, 307);
            this.lblMontantTotalReservations.Name = "lblMontantTotalReservations";
            this.lblMontantTotalReservations.Size = new System.Drawing.Size(16, 18);
            this.lblMontantTotalReservations.TabIndex = 79;
            this.lblMontantTotalReservations.Text = "€";
            // 
            // lblNombreTotalReservations
            // 
            this.lblNombreTotalReservations.AutoSize = true;
            this.lblNombreTotalReservations.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.lblNombreTotalReservations.Location = new System.Drawing.Point(295, 360);
            this.lblNombreTotalReservations.Name = "lblNombreTotalReservations";
            this.lblNombreTotalReservations.Size = new System.Drawing.Size(0, 18);
            this.lblNombreTotalReservations.TabIndex = 80;
            // 
            // frmFacturationReservations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1070, 440);
            this.Controls.Add(this.lblNombreTotalReservations);
            this.Controls.Add(this.lblMontantTotalReservations);
            this.Controls.Add(this.lblNombreReservations);
            this.Controls.Add(this.lblMontant);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.dtpMoisReservations);
            this.Controls.Add(this.lblLiguesReservations);
            this.Controls.Add(this.cbxLigueReservations);
            this.Controls.Add(this.lblMoisReservations);
            this.Controls.Add(this.label1);
            this.Name = "frmFacturationReservations";
            this.Text = "Facturation Reservations";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblMoisReservations;
        private System.Windows.Forms.ComboBox cbxLigueReservations;
        private System.Windows.Forms.Label lblLiguesReservations;
        private System.Windows.Forms.DateTimePicker dtpMoisReservations;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label lblMontant;
        private System.Windows.Forms.Label lblNombreReservations;
        private System.Windows.Forms.Label lblMontantTotalReservations;
        private System.Windows.Forms.Label lblNombreTotalReservations;
    }
}