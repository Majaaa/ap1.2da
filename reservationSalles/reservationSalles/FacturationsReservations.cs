﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace reservationSalles2018
{
    public partial class frmReservations : Form
    {
       

        public frmReservations()
        {
            InitializeComponent();
        }

        private void frmFacturationReservations_Load(object sender, EventArgs e)
        {
            dtpReservations.Format = DateTimePickerFormat.Custom;
            dtpReservations.CustomFormat = "MM/yyyy";
        }
    }
}
