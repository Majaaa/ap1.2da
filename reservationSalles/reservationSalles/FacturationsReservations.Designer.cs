﻿namespace reservationSalles2018
{
    partial class frmReservations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.gdvReservations = new System.Windows.Forms.DataGridView();
            this.cbxLiguesReservations = new System.Windows.Forms.ComboBox();
            this.dtpReservations = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.gdvReservations)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(313, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(349, 33);
            this.label1.TabIndex = 71;
            this.label1.Text = "Réservations des ligues";
            // 
            // gdvReservations
            // 
            this.gdvReservations.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gdvReservations.Location = new System.Drawing.Point(43, 173);
            this.gdvReservations.Name = "gdvReservations";
            this.gdvReservations.Size = new System.Drawing.Size(886, 257);
            this.gdvReservations.TabIndex = 72;
            // 
            // cbxLiguesReservations
            // 
            this.cbxLiguesReservations.FormattingEnabled = true;
            this.cbxLiguesReservations.Location = new System.Drawing.Point(222, 98);
            this.cbxLiguesReservations.Name = "cbxLiguesReservations";
            this.cbxLiguesReservations.Size = new System.Drawing.Size(179, 21);
            this.cbxLiguesReservations.TabIndex = 73;
            // 
            // dtpReservations
            // 
            this.dtpReservations.Location = new System.Drawing.Point(578, 99);
            this.dtpReservations.MaxDate = new System.DateTime(2050, 12, 31, 0, 0, 0, 0);
            this.dtpReservations.MinDate = new System.DateTime(1990, 1, 1, 0, 0, 0, 0);
            this.dtpReservations.Name = "dtpReservations";
            this.dtpReservations.Size = new System.Drawing.Size(200, 20);
            this.dtpReservations.TabIndex = 75;
            // 
            // frmReservations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(964, 463);
            this.Controls.Add(this.dtpReservations);
            this.Controls.Add(this.cbxLiguesReservations);
            this.Controls.Add(this.gdvReservations);
            this.Controls.Add(this.label1);
            this.Name = "frmReservations";
            this.Text = "Reservations";
            this.Load += new System.EventHandler(this.frmFacturationReservations_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gdvReservations)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView gdvReservations;
        private System.Windows.Forms.ComboBox cbxLiguesReservations;
        private System.Windows.Forms.DateTimePicker dtpReservations;
    }
}