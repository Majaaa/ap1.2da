﻿
namespace reservationSalles2018
{
    partial class frmStatistiquesTypes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lblMois = new System.Windows.Forms.Label();
            this.cbxMois = new System.Windows.Forms.ComboBox();
            this.lblSalles = new System.Windows.Forms.Label();
            this.dtpMois = new System.Windows.Forms.DateTimePicker();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.lblMontant = new System.Windows.Forms.Label();
            this.lblMontantTotalReservationsSalles = new System.Windows.Forms.Label();
            this.lblResservationsSalles = new System.Windows.Forms.Label();
            this.lblNombreTotalReservationsSalles = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(244, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(434, 33);
            this.label1.TabIndex = 72;
            this.label1.Text = "Statistiques par type de salles";
            // 
            // lblMois
            // 
            this.lblMois.AutoSize = true;
            this.lblMois.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.lblMois.Location = new System.Drawing.Point(56, 92);
            this.lblMois.Name = "lblMois";
            this.lblMois.Size = new System.Drawing.Size(142, 18);
            this.lblMois.TabIndex = 73;
            this.lblMois.Text = "Choisissez le mois :";
            // 
            // cbxMois
            // 
            this.cbxMois.FormattingEnabled = true;
            this.cbxMois.Location = new System.Drawing.Point(59, 127);
            this.cbxMois.Name = "cbxMois";
            this.cbxMois.Size = new System.Drawing.Size(220, 21);
            this.cbxMois.TabIndex = 74;
            // 
            // lblSalles
            // 
            this.lblSalles.AutoSize = true;
            this.lblSalles.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.lblSalles.Location = new System.Drawing.Point(56, 191);
            this.lblSalles.Name = "lblSalles";
            this.lblSalles.Size = new System.Drawing.Size(139, 18);
            this.lblSalles.TabIndex = 75;
            this.lblSalles.Text = "Choisissez la salle :";
            // 
            // dtpMois
            // 
            this.dtpMois.Location = new System.Drawing.Point(56, 228);
            this.dtpMois.Name = "dtpMois";
            this.dtpMois.Size = new System.Drawing.Size(200, 20);
            this.dtpMois.TabIndex = 76;
            this.dtpMois.ValueChanged += new System.EventHandler(this.dtpMois_ValueChanged);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(390, 92);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(661, 367);
            this.dataGridView1.TabIndex = 77;
            // 
            // lblMontant
            // 
            this.lblMontant.AutoSize = true;
            this.lblMontant.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.lblMontant.Location = new System.Drawing.Point(53, 346);
            this.lblMontant.Name = "lblMontant";
            this.lblMontant.Size = new System.Drawing.Size(215, 18);
            this.lblMontant.TabIndex = 78;
            this.lblMontant.Text = "Montant total des réservations :";
            // 
            // lblMontantTotalReservationsSalles
            // 
            this.lblMontantTotalReservationsSalles.AutoSize = true;
            this.lblMontantTotalReservationsSalles.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.lblMontantTotalReservationsSalles.Location = new System.Drawing.Point(289, 346);
            this.lblMontantTotalReservationsSalles.Name = "lblMontantTotalReservationsSalles";
            this.lblMontantTotalReservationsSalles.Size = new System.Drawing.Size(16, 18);
            this.lblMontantTotalReservationsSalles.TabIndex = 79;
            this.lblMontantTotalReservationsSalles.Text = "€";
            // 
            // lblResservationsSalles
            // 
            this.lblResservationsSalles.AutoSize = true;
            this.lblResservationsSalles.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.lblResservationsSalles.Location = new System.Drawing.Point(53, 407);
            this.lblResservationsSalles.Name = "lblResservationsSalles";
            this.lblResservationsSalles.Size = new System.Drawing.Size(223, 18);
            this.lblResservationsSalles.TabIndex = 80;
            this.lblResservationsSalles.Text = "Nombres total des réservations :";
            // 
            // lblNombreTotalReservationsSalles
            // 
            this.lblNombreTotalReservationsSalles.AutoSize = true;
            this.lblNombreTotalReservationsSalles.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.lblNombreTotalReservationsSalles.Location = new System.Drawing.Point(289, 407);
            this.lblNombreTotalReservationsSalles.Name = "lblNombreTotalReservationsSalles";
            this.lblNombreTotalReservationsSalles.Size = new System.Drawing.Size(0, 18);
            this.lblNombreTotalReservationsSalles.TabIndex = 81;
            // 
            // frmStatistiquesTypes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1091, 563);
            this.Controls.Add(this.lblNombreTotalReservationsSalles);
            this.Controls.Add(this.lblResservationsSalles);
            this.Controls.Add(this.lblMontantTotalReservationsSalles);
            this.Controls.Add(this.lblMontant);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.dtpMois);
            this.Controls.Add(this.lblSalles);
            this.Controls.Add(this.cbxMois);
            this.Controls.Add(this.lblMois);
            this.Controls.Add(this.label1);
            this.Name = "frmStatistiquesTypes";
            this.Text = "statistiquesTypes";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblMois;
        private System.Windows.Forms.ComboBox cbxMois;
        private System.Windows.Forms.Label lblSalles;
        private System.Windows.Forms.DateTimePicker dtpMois;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label lblMontant;
        private System.Windows.Forms.Label lblMontantTotalReservationsSalles;
        private System.Windows.Forms.Label lblResservationsSalles;
        private System.Windows.Forms.Label lblNombreTotalReservationsSalles;
    }
}