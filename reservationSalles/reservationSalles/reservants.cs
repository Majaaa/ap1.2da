﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace reservationSalles2018
{
    public partial class frmReservants : Form
    {
        DataTable tableReservants = frmM2LReservationSalles.reservationsSallesDataSet.Tables["Reservants"];
        DataTable tableLigues = frmM2LReservationSalles.reservationsSallesDataSet.Tables["ligues"];
        Boolean enregModifierReservant;
       

        DataRelation reservantsLigues = frmM2LReservationSalles.reservationsSallesDataSet.Relations["EquiJoinReservantLigue"];

        public frmReservants()
        {
            InitializeComponent();
           
        }

        private void reservants_Load(object sender, EventArgs e)
        {

            // limite le nombre de caractère de tbxRechercherReservant.
            tbxRechercherReservant.MaxLength = 40;
            // limite le nombre de caractère de tbxInseeReservant.
            tbxInseeReservant.MaxLength = 15;
            // limite le nombre de caractère de tbxNomReservant.
            tbxNomReservant.MaxLength = 40;
            // limite le nombre de caractère de tbxPrenomReservant.
            tbxPrenomReservant.MaxLength = 40;
            // limite le nombre de caractère de tbxTelephoneReservant.
            tbxTelephoneReservant.MaxLength = 10;
            // limite le nombre de caractère de tbxMailReservant.
            tbxMailReservant.MaxLength = 20;
            // limite de nombre de caractère de tbxFonctionReservant.
            tbxFonctionReservant.MaxLength = 20;




            lbxReservants.DataSource = tableReservants;
            lbxReservants.DisplayMember = "nomPrenom";
            lbxReservants.ValueMember = "idReservant";
        

            cbxLigueReservant.DataSource = tableLigues;
            cbxLigueReservant.DisplayMember = tableLigues.Columns[1].ToString();
            cbxLigueReservant.ValueMember = tableLigues.Columns[0].ToString();


            tbxInseeReservant.Enabled = false;
            tbxNomReservant.Enabled = false;
            tbxPrenomReservant.Enabled = false;
            tbxTelephoneReservant.Enabled = false;
            tbxMailReservant.Enabled = false;
            cbxLigueReservant.Enabled = false;
            tbxFonctionReservant.Enabled = false;

            btnEnregistrerReservant.Visible = false;
            btnAnnulerReservant.Visible = false;

            btnRechercherReservant.Enabled = true;
        }

        private void lbxReservants_SelectedIndexChanged(object sender, EventArgs e)
        {


            if (lbxReservants.SelectedIndex != -1)
            {
                tbxInseeReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[1].ToString();
                tbxNomReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[2].ToString();
                tbxPrenomReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[3].ToString();
                tbxTelephoneReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[4].ToString();
                tbxMailReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[5].ToString();
                tbxFonctionReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[7].ToString();

                
                cbxLigueReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].GetParentRow(reservantsLigues)["nomLigue"].ToString();
                
            }
        }

        private void btnRechercherReservant_Click(object sender, EventArgs e)
        {
            int index = lbxReservants.FindString(tbxRechercherReservant.Text);
            if (index == -1)
            {
                MessageBox.Show("Reéservant introuvable.");
            }
            else
            {
                lbxReservants.SetSelected(index, true);
            }
        }


        private void btnModifierReservant_Click(object sender, EventArgs e)
        {
            if (tbxNomReservant.Text != "")
            {
                enregModifierReservant = true;
                tbxInseeReservant.Enabled = true;
                tbxNomReservant.Enabled = true;
                tbxPrenomReservant.Enabled = true;
                tbxTelephoneReservant.Enabled = true;
                tbxMailReservant.Enabled = true;
                cbxLigueReservant.Enabled = true;
                tbxFonctionReservant.Enabled = true;

                btnAjouterReservant.Visible = false;
                btnSupprimerReservant.Visible = false;
                btnModifierReservant.Enabled = false;

                btnEnregistrerReservant.Visible = true;
                btnAnnulerReservant.Visible = true;

                lbxReservants.Enabled = false;
                tbxRechercherReservant.Enabled = false;
                btnRechercherReservant.Enabled = false;
            }
        }

        private void btnAnnulerReservant_Click(object sender, EventArgs e)
        {

            enregModifierReservant = false;

            tbxInseeReservant.Enabled = false;
            tbxNomReservant.Enabled = false;
            tbxPrenomReservant.Enabled = false;
            tbxTelephoneReservant.Enabled = false;
            tbxMailReservant.Enabled = false;
            cbxLigueReservant.Enabled = false;
            tbxFonctionReservant.Enabled = false;

            btnAjouterReservant.Visible = true;
            btnSupprimerReservant.Visible = true;
            btnModifierReservant.Enabled = true;

            btnEnregistrerReservant.Visible = false;
            btnAnnulerReservant.Visible = false;

            lbxReservants.Enabled = true;
            tbxRechercherReservant.Enabled = true;
            btnRechercherReservant.Enabled = true;

            if (lbxReservants.SelectedIndex != -1)
            {
                tbxInseeReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[1].ToString();
                tbxNomReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[2].ToString();
                tbxPrenomReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[3].ToString();
                tbxTelephoneReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[4].ToString();
                tbxMailReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[5].ToString();
                tbxFonctionReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[7].ToString();


                cbxLigueReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].GetParentRow(reservantsLigues)["nomLigue"].ToString();

            }
        }


        private void btnAjouterReservant_Click(object sender, EventArgs e)
        {
            String insee = tbxInseeReservant.Text;
            Regex rx = new Regex(@"^([1-2]{1}|[1-9]{2-15}$)");
            if (!rx.IsMatch(insee))
            {
                tbxInseeReservant.Text = null;
                tbxInseeReservant.Focus();
                MessageBox.Show("Code Insee invalide, il doit commencer par un 1 ou par un 2.");
            }



            Regex reg = new Regex(@"^([\w]{1,40})@(\w{1,40}).(.{2,5})$");


            if (!reg.IsMatch(this.tbxMailReservant.Text))
            {
                tbxMailReservant.Text = null;
                tbxMailReservant.Focus();
                MessageBox.Show("Le format de l'adresse email est incorrect.");
            }

            tbxInseeReservant.Text = "";
            tbxNomReservant.Text = "";
            tbxPrenomReservant.Text = "";
            tbxTelephoneReservant.Text = "";
            tbxMailReservant.Text = "";
            tbxFonctionReservant.Text = "";
            cbxLigueReservant.SelectedIndex = 0;

            btnRechercherReservant.Enabled = false;
            tbxRechercherReservant.Enabled = false;

            btnAjouterReservant.Visible = false;
            btnSupprimerReservant.Visible = false;
            btnModifierReservant.Enabled = false;

            lbxReservants.Enabled = false;

            btnEnregistrerReservant.Visible = true;
            btnAnnulerReservant.Visible = true;


            enregModifierReservant = false;
            tbxInseeReservant.Enabled = true;
            tbxNomReservant.Enabled = true;
            tbxPrenomReservant.Enabled = true;
            tbxTelephoneReservant.Enabled = true;
            tbxMailReservant.Enabled = true;
            cbxLigueReservant.Enabled = true;
            tbxFonctionReservant.Enabled = true;
        }

        private void btnEnregistrerReservant_Click(object sender, EventArgs e)
        {

            
            int idReservant;
            short indice;

            indice = 0;


            tbxInseeReservant.Text = tbxInseeReservant.Text.Trim();
            tbxNomReservant.Text = tbxNomReservant.Text.Trim();
            tbxPrenomReservant.Text = tbxPrenomReservant.Text.Trim();
            tbxTelephoneReservant.Text = tbxTelephoneReservant.Text.Trim();
            tbxMailReservant.Text = tbxMailReservant.Text.Trim();
            tbxFonctionReservant.Text = tbxFonctionReservant.Text.Trim();


            


                try
                {

                    if (enregModifierReservant == false)
                    {
                        dbConnexion.ajouterReservant(tbxInseeReservant.Text, tbxNomReservant.Text, tbxPrenomReservant.Text, tbxTelephoneReservant.Text, tbxMailReservant.Text, Convert.ToInt32(cbxLigueReservant.SelectedValue), tbxFonctionReservant.Text);
                    }
                    else
                    {
                        idReservant = Convert.ToInt32(tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[0]);
                        dbConnexion.modifierReservant(idReservant, tbxInseeReservant.Text, tbxNomReservant.Text, tbxPrenomReservant.Text, tbxTelephoneReservant.Text, tbxMailReservant.Text, Convert.ToInt32(cbxLigueReservant.SelectedValue), tbxFonctionReservant.Text);

                        indice = Convert.ToInt16(lbxReservants.SelectedIndex);

                    }

                    dbConnexion.miseJourDataSet();
                  
                    lbxReservants.DataSource = tableReservants;
                    lbxReservants.DisplayMember = "nomPrenom";


                    if (enregModifierReservant == true)
                    {
                        lbxReservants.SelectedIndex = indice;
                        enregModifierReservant = false;
                    }
                    else
                    {
                        lbxReservants.SelectedIndex = lbxReservants.Items.Count - 1;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                tbxInseeReservant.Enabled = false;
                tbxNomReservant.Enabled = false;
                tbxPrenomReservant.Enabled = false;
                tbxTelephoneReservant.Enabled = false;
                tbxMailReservant.Enabled = false;
                cbxLigueReservant.Enabled = false;
                tbxFonctionReservant.Enabled = false;

                btnAjouterReservant.Visible = true;
                btnSupprimerReservant.Visible = true;
                btnModifierReservant.Enabled = true;

                btnEnregistrerReservant.Visible = false;
                btnAnnulerReservant.Visible = false;

                lbxReservants.Enabled = true;
                tbxRechercherReservant.Enabled = true;
                btnRechercherReservant.Enabled = true;

                if (lbxReservants.SelectedIndex != -1)
                {
                    tbxInseeReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[1].ToString();
                    tbxNomReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[2].ToString();
                    tbxPrenomReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[3].ToString();
                    tbxTelephoneReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[4].ToString();
                    tbxMailReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[5].ToString();
                    tbxFonctionReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[7].ToString();


                    cbxLigueReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].GetParentRow(reservantsLigues)["nomLigue"].ToString();

                }

            String insee = tbxInseeReservant.Text;
            Regex rx = new Regex(@"^([1-2]{1}|[1-9]{2-15}$)");
            if (!rx.IsMatch(insee))
            {
                tbxInseeReservant.Text = null;
                tbxInseeReservant.Focus();
                MessageBox.Show("Code Insee invalide, il doit commencer par un 1 ou par un 2.");
            }



            Regex reg = new Regex(@"^([\w]{1,40})@(\w{1,40}).(.{2,5})$");


            if (!reg.IsMatch(this.tbxMailReservant.Text))
            {
                tbxMailReservant.Text = null;
                tbxMailReservant.Focus();
                MessageBox.Show("Le format de l'adresse email est incorrect.");
            }

        }

        private void btnSupprimerReservant_Click(object sender, EventArgs e)
        {



            int idReservant;

            string message = "Etes vous sûr de vouloir supprimer ce réservant ?";
            string title = "Etes-vous sûr ?";

            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result = MessageBox.Show(message, title, buttons);

            if (result == DialogResult.Yes)
            {
                if (lbxReservants.SelectedIndex >= 0)
                {
                    idReservant = Convert.ToInt32(tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[0]);
                    dbConnexion.supprimerReservant(idReservant);
                    dbConnexion.miseJourDataSet();

                    if (lbxReservants.SelectedIndex != -1)
                    {
                        tbxInseeReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[1].ToString();
                        tbxNomReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[2].ToString();
                        tbxPrenomReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[3].ToString();
                        tbxTelephoneReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[4].ToString();
                        tbxMailReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[5].ToString();
                        tbxFonctionReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[7].ToString();

                        cbxLigueReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].GetParentRow(reservantsLigues)["nomLigue"].ToString();
                    }
                    else
                    {
                        tbxInseeReservant.Text = "";
                        tbxNomReservant.Text = "";
                        tbxPrenomReservant.Text = "";
                        tbxTelephoneReservant.Text = "";
                        tbxMailReservant.Text = "";
                        tbxFonctionReservant.Text = "";
                        cbxLigueReservant.SelectedIndex = 0;

                    }

                }
                else
                {
                    MessageBox.Show("Vous devez sélectionner un réservant");
                }
            }



        }

        private void btnRechercherReservant_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '-' && e.KeyChar != '\'' && e.KeyChar != (char)Keys.Space)
            {
                e.Handled = true;
            }
        }

        private void tbxInseeReservant_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != 'A' && e.KeyChar != 'B' && e.KeyChar != (char)Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void tbxNomReservant_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '-' && e.KeyChar != '\'' && e.KeyChar != (char)Keys.Space)
            {
                e.Handled = true;
            }
        }

        private void tbxPrenomReservant_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '-' && e.KeyChar != '\'' && e.KeyChar != (char)Keys.Space)
            {
                e.Handled = true;
            }
        }

        private void tbxTelephoneReservant_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void tbxFonctionReservant_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != '-' && e.KeyChar != '\'' && e.KeyChar != (char)Keys.Space)
            {
                e.Handled = true;
            }
        }
    }
}
